let registerForm = document.querySelector("#registerUser")

//a user interaction such as loading the page, clicking on an HTML element, typing sth,
// or submitting a form is called an EVENT 

//since we do not want the default behavior of our form submission event to happen, we must prevent that default behaviour

//by using addEventListener to bind our code to the form submission event, we can do that.


registerForm.addEventListener("submit", (e) =>{
	//e in the parameters refets to the event itself (e is a placeholder name and can be anything)

	e.preventDefault() //precentDefault prevents our form from reloading the page when submitted
	let firstName = document.querySelector("#firstName").value
	let lastName = document.querySelector("#lastName").value
	let mobileNo = document.querySelector("#mobileNumber").value
	let email = document.querySelector("#userEmail").value
	let password1 = document.querySelector("#password1").value
	let password2 = document.querySelector("#password2").value

	//validation to enable form submission only when all fields are populated,
	//when passwords match, and when mobile number is exactly 11 numbers long
	//Always validate these password in Javascript for security reason, there is a way pass require on HTML with ""
	if((password1 !== '' && password2 !== '') && (password1 === password2) && (mobileNo.length === 11)) {
		// alert(`User ${firstName} ${lastName} with email ${email} and mobile number ${mobileNo} successfully registerd.`)
		fetch('http://localhost:4000/users/checkEmail',{
			//Anything that is not a GET request, you need to add the method
			// You need to do the same things in Postman in Fetch
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(res => res.json())
		.then(data =>{
			// console.log(`It is ${data} that there is a duplicate email`)
			if(data) {
				alert("Duplicate email found. Please use different email address.")
			}else {
				//ACTIVITY
				// Create a fetch request in this else statement that allows our user to register
				// Properly handle the response so that a successful registration shows and alert thatsays 
				// registrration is successful, and an alert that says registration is NOT successful if not
				fetch('http://localhost:4000/users/register', {
					method: 'POST',
					headers: {
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						mobileNumber: mobileNo,
						email: email,
						password: password1

					})	
				})
				.then(res => res.json())
				.then(data => console.log(data))
				alert("Successfully register")
			}
		})	
	} else {
		alert ("Please check your registration details and try again.")
	}

})



	











